package api.sms.astro.core.transaction.domaine;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class GrilleTarifaire implements Serializable {

  private UUID id;

  private BigDecimal montantMin;
  private BigDecimal montantMax;
  private BigDecimal fraisRetrait;
  private BigDecimal fraisDepot;
  private BigDecimal timbreEtat;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
