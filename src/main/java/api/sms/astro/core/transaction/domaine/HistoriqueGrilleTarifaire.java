package api.sms.astro.core.transaction.domaine;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueGrilleTarifaire {

  private UUID id;

  private LocalDateTime dateHistorisation;

  private Set<GrilleTarifaire> grilleTarifaires;
}
