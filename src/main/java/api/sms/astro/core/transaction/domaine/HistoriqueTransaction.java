package api.sms.astro.core.transaction.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueTransaction implements Serializable {

  private UUID id;
  private Transaction transaction;
  private Utilisateur utilisateur;


  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public HistoriqueTransaction() {
  }

  public HistoriqueTransaction(
      Transaction transaction,
      Utilisateur utilisateur) {
    this.transaction = transaction;
    this.utilisateur = utilisateur;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HistoriqueTransaction that = (HistoriqueTransaction) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(transaction, that.transaction) &&
        Objects.equals(utilisateur, that.utilisateur) &&
        Objects.equals(dateCreation, that.dateCreation) &&
        Objects.equals(dateModification, that.dateModification);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(id, transaction, utilisateur, dateCreation, dateModification);
  }

  @Override
  public String toString() {
    return "HistoriqueTransaction{" +
        "id=" + id +
        ", transaction=" + transaction +
        ", utilisateur=" + utilisateur +
        ", dateCreation=" + dateCreation +
        ", dateModification=" + dateModification +
        '}';
  }
}
