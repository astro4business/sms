package api.sms.astro.core.transaction.domaine;

import api.sms.astro.core.contact.domaine.Contact;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Prestataire implements Serializable {

  private UUID id;

  private String nom;

  private Contact contact;

  private Set<GrilleTarifaire> grilleTarifaire;
  private Set<HistoriqueGrilleTarifaire> historiqueGrilleTarifaire;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
