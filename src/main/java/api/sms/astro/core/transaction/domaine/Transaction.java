package api.sms.astro.core.transaction.domaine;

import api.sms.astro.core.campagne.domaine.Campagne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Transaction {

  private UUID id;

  private BigDecimal fraisTransaction;

  private Prestataire prestataire;
  private Campagne campagne;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
