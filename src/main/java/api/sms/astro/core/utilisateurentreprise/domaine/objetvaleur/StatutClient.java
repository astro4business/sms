package api.sms.astro.core.utilisateurentreprise.domaine.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum StatutClient {
  DIAMAND,
  OR,
  ARGENT,
  BRONZE
}
