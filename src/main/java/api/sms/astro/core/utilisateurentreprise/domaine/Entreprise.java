package api.sms.astro.core.utilisateurentreprise.domaine;

import api.sms.astro.core.campagne.domaine.Campagne;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.core.utilisateurentreprise.domaine.objetvaleur.StatutClient;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Entreprise extends Utilisateur {

  private StatutClient statutClient = StatutClient.BRONZE;
  private String description;
  private String raisonSociale;
  private String siret;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  private Set<MembreEntreprise> membreEntreprise;
  private Set<Contact> contacts;

  private Set<Campagne> campagnes;
  private Set<Transaction> transactions;

  private Set<GroupeProspect> groupeProspects;

  public Entreprise(String nom, String motPasse) {
    super(nom, motPasse);
  }

  public StatutClient getStatutClient() {
    return statutClient;
  }

  public void setStatutClient(
      StatutClient statutClient) {
    this.statutClient = statutClient;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRaisonSociale() {
    return raisonSociale;
  }

  public void setRaisonSociale(String raisonSociale) {
    this.raisonSociale = raisonSociale;
  }

  public String getSiret() {
    return siret;
  }

  public void setSiret(String siret) {
    this.siret = siret;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  public Set<MembreEntreprise> getMembreEntreprise() {
    return membreEntreprise;
  }

  public void setMembreEntreprise(
      Set<MembreEntreprise> membreEntreprise) {
    this.membreEntreprise = membreEntreprise;
  }

  public Set<Contact> getContacts() {
    return contacts;
  }

  public void setContacts(
      Set<Contact> contacts) {
    this.contacts = contacts;
  }

  @Override
  public Set<Campagne> getCampagnes() {
    return campagnes;
  }

  public void setCampagnes(
      Set<Campagne> campagnes) {
    this.campagnes = campagnes;
  }

  public Set<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(
      Set<Transaction> transactions) {
    this.transactions = transactions;
  }

  @Override
  public Set<GroupeProspect> getGroupeProspects() {
    return groupeProspects;
  }

  public void setGroupeProspects(
      Set<GroupeProspect> groupeProspects) {
    this.groupeProspects = groupeProspects;
  }
}
