package api.sms.astro.core.utilisateurentreprise.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.core.utilisateurentreprise.domaine.objetvaleur.StatutClient;
import java.time.LocalDateTime;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class MembreEntreprise extends Utilisateur {

  private LocalDateTime dateAdhesion;
  private StatutClient statutClient = StatutClient.BRONZE;

  public MembreEntreprise(String nom, String prenoms, String motPasse) {
    super(nom, prenoms, motPasse);
  }

  public LocalDateTime getDateAdhesion() {
    return dateAdhesion;
  }

  public void setDateAdhesion(LocalDateTime dateAdhesion) {
    this.dateAdhesion = dateAdhesion;
  }

  public StatutClient getStatutClient() {
    return statutClient;
  }

  public void setStatutClient(
      StatutClient statutClient) {
    this.statutClient = statutClient;
  }
}
