package api.sms.astro.core.campagne.domaine;

import api.sms.astro.core.campagne.domaine.objetvaleur.Audience;
import api.sms.astro.core.campagne.domaine.objetvaleur.ModeEnvoie;
import api.sms.astro.core.campagne.domaine.objetvaleur.PeriodeCampagne;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Campagne {

  private UUID id;
  private String intitule;
  private String numeroCampagne;

  private Utilisateur utilisateur;

  private PeriodeCampagne periodeCampagne;
  private Audience audience;
  private Set<GroupeProspect> groupeProspects;

  private MessageCampagne messageCampagne;
  private final ModeEnvoie modeEnvoie = ModeEnvoie.INSTANTANEE;

  private Transaction transaction;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public Campagne() {
    super();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getIntitule() {
    return intitule;
  }

  public void setIntitule(String intitule) {
    this.intitule = intitule;
  }

  public String getNumeroCampagne() {
    return numeroCampagne;
  }

  public void setNumeroCampagne(String numeroCampagne) {
    this.numeroCampagne = numeroCampagne;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public PeriodeCampagne getPeriodeCampagne() {
    return periodeCampagne;
  }

  public void setPeriodeCampagne(
      PeriodeCampagne periodeCampagne) {
    this.periodeCampagne = periodeCampagne;
  }

  public Audience getAudience() {
    return audience;
  }

  public void setAudience(Audience audience) {
    this.audience = audience;
  }

  public Set<GroupeProspect> getGroupeProspects() {
    return groupeProspects;
  }

  public void setGroupeProspects(
      Set<GroupeProspect> groupeProspects) {
    this.groupeProspects = groupeProspects;
  }

  public MessageCampagne getMessageCampagne() {
    return messageCampagne;
  }

  public void setMessageCampagne(
      MessageCampagne messageCampagne) {
    this.messageCampagne = messageCampagne;
  }

  public ModeEnvoie getModeEnvoie() {
    return modeEnvoie;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Campagne campagne = (Campagne) o;
    return Objects.equals(id, campagne.id) &&
        Objects.equals(intitule, campagne.intitule) &&
        Objects.equals(numeroCampagne, campagne.numeroCampagne) &&
        Objects.equals(utilisateur, campagne.utilisateur) &&
        Objects.equals(periodeCampagne, campagne.periodeCampagne) &&
        Objects.equals(audience, campagne.audience) &&
        Objects.equals(groupeProspects, campagne.groupeProspects) &&
        Objects.equals(messageCampagne, campagne.messageCampagne) &&
        modeEnvoie == campagne.modeEnvoie &&
        Objects.equals(transaction, campagne.transaction) &&
        Objects.equals(dateCreation, campagne.dateCreation) &&
        Objects.equals(dateModification, campagne.dateModification);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(id, intitule, numeroCampagne, utilisateur, periodeCampagne, audience,
            groupeProspects, messageCampagne, modeEnvoie, transaction, dateCreation,
            dateModification);
  }

  @Override
  public String toString() {
    return "Campagne{" +
        "id=" + id +
        ", intitule='" + intitule + '\'' +
        ", numeroCampagne='" + numeroCampagne + '\'' +
        ", utilisateur=" + utilisateur +
        ", periodeCampagne=" + periodeCampagne +
        ", audience=" + audience +
        ", groupeProspects=" + groupeProspects +
        ", messageCampagne=" + messageCampagne +
        ", modeEnvoie=" + modeEnvoie +
        ", transaction=" + transaction +
        ", dateCreation=" + dateCreation +
        ", dateModification=" + dateModification +
        '}';
  }
}
