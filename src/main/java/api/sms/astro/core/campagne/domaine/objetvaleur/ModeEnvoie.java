package api.sms.astro.core.campagne.domaine.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum ModeEnvoie {
  INSTANTANEE,
  DIFFERE
}
