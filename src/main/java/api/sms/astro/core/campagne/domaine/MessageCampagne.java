package api.sms.astro.core.campagne.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.util.Date;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class MessageCampagne {

  private UUID id;

  private String intitule;
  private String messageFormate;

  private Utilisateur destinataire;

  private Date dateCreation;
  private Date dateModification;

  public MessageCampagne() {
    super();
  }
}
