package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class SupprimerPays {

  private final PaysPortRepository paysPortRepository;

  public SupprimerPays(
      PaysPortRepository paysPortRepository) {
    this.paysPortRepository = paysPortRepository;
  }

  public void supprimer(Pays pays) {
    this.paysPortRepository.supprimer(pays);
  }
}
