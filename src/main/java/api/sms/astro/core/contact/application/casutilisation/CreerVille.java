package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.VillePortRepository;
import api.sms.astro.core.contact.domaine.Ville;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/

// Todo : à supprimer
public class CreerVille {

  private final VillePortRepository villePortRepository;

  public CreerVille(
      VillePortRepository villePortRepository) {
    this.villePortRepository = villePortRepository;
  }
  
  public void creer(Ville ville) {
    this.villePortRepository.enregistrer(ville);
  }
}
