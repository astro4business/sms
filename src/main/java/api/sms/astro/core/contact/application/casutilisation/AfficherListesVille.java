package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.VillePortRepository;
import api.sms.astro.core.contact.domaine.Ville;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class AfficherListesVille {

  private final VillePortRepository villePortRepository;

  public AfficherListesVille(
      VillePortRepository villePortRepository) {
    this.villePortRepository = villePortRepository;
  }

  public List<Ville> afficher() {
    return this.villePortRepository.afficherListes();
  }
}
