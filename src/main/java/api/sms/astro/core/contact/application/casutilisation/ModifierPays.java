package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class ModifierPays {

  private final PaysPortRepository paysPortRepository;

  public ModifierPays(
      PaysPortRepository paysPortRepository) {
    this.paysPortRepository = paysPortRepository;
  }

  public void modifier(Pays pays) {

    pays.getVilles().forEach(p -> p.setPays(pays));
    this.paysPortRepository.enregistrer(pays);
  }
}
