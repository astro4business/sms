package api.sms.astro.core.contact.application.commande;

import java.util.UUID;
import javax.validation.constraints.NotBlank;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public class ModifierContactCommande extends CreerContactCommande {

  @NotBlank(message = "L'identifiant du contact est incorrect.")
  private UUID id;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }
}
