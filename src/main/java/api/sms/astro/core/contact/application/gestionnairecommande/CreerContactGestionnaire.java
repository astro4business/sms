package api.sms.astro.core.contact.application.gestionnairecommande;

import api.sms.astro.core.contact.application.casutilisation.CreerContact;
import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.utilisateur.GestionnaireCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
public class CreerContactGestionnaire implements GestionnaireCommande<CreerContactCommande> {

  private final CreerContact creerContact;

  public CreerContactGestionnaire(
      ContactPortRepository contactPortRepository) {
    this.creerContact = new CreerContact(contactPortRepository);
  }

  @Override
  public void execute(CreerContactCommande commande) {
    this.creerContact.creer(commande);
  }
}
