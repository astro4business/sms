package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class AfficherListesPays {

  private final PaysPortRepository paysPortRepository;

  public AfficherListesPays(
      PaysPortRepository paysPortRepository) {
    this.paysPortRepository = paysPortRepository;
  }

  public List<Pays> afficher() {
    return this.paysPortRepository.afficherListes();
  }
}
