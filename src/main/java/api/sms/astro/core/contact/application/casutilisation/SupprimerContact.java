package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.contact.application.port.ContactPortRepository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class SupprimerContact {
  
  private final ContactPortRepository contactPortRepository;
  
  public SupprimerContact(
      ContactPortRepository contactPortRepository) {
    this.contactPortRepository = contactPortRepository;
  }
  
  public void supprimer(Contact contact) {
    this.contactPortRepository.supprimer(contact);
  }
}
