package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.VillePortRepository;
import api.sms.astro.core.contact.domaine.Ville;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class ModifierVille {

  private final VillePortRepository villePortRepository;

  public ModifierVille(
      VillePortRepository villePortRepository) {
    this.villePortRepository = villePortRepository;
  }

  public void modifier(Ville ville) {
    this.villePortRepository.enregistrer(ville);
  }
}
