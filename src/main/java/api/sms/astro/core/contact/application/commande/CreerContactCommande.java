package api.sms.astro.core.contact.application.commande;

import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
public class CreerContactCommande {

  private UUID id;

  @Email(message = "L'adresse email est incorrect.")
  private String email;

  @NotBlank(message = "Le numéro de téléphone est incorrect.")
  @Size(min = 3)
  private String numeroTelephone;

  @NotNull(message = "Le numéro fixe est incorrect.")
  @Size(min = 3)
  private String numeroFixe;

  @NotNull(message = "L'adresse postal est incorrect.")
  @Size(min = 3)
  private String adressePostal;

  @NotBlank(message = "Le nom du pays est incorrect.")
  private Pays pays;

  @NotBlank(message = "La nom de la ville est incorrect.")
  private Ville ville;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNumeroTelephone() {
    return numeroTelephone;
  }

  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public String getNumeroFixe() {
    return numeroFixe;
  }

  public void setNumeroFixe(String numeroFixe) {
    this.numeroFixe = numeroFixe;
  }

  public String getAdressePostal() {
    return adressePostal;
  }

  public void setAdressePostal(String adressePostal) {
    this.adressePostal = adressePostal;
  }

  public Pays getPays() {
    return pays;
  }

  public void setPays(Pays pays) {
    this.pays = pays;
  }

  public Ville getVille() {
    return ville;
  }

  public void setVille(Ville ville) {
    this.ville = ville;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }
}
