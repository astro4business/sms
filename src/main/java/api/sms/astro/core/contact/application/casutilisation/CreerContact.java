package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Adresse;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
public class CreerContact {

  protected final ContactPortRepository contactPortRepository;

  public CreerContact(
      ContactPortRepository contactPortRepository) {
    this.contactPortRepository = contactPortRepository;
  }

  public Contact genererContact(CreerContactCommande commande) {

    String email = commande.getEmail();
    String numeroTelephone = commande.getNumeroTelephone();
    String numeroFixe = commande.getNumeroFixe();

    Contact contact = new Contact(email, numeroTelephone, numeroFixe);

    String adressePostal = commande.getAdressePostal();
    Pays pays = commande.getPays();
    Ville ville = commande.getVille();

    Adresse adresse;

    if (pays.getVilles().contains(ville)) {
      adresse = new Adresse(adressePostal, pays, ville);
      contact.setAdresse(adresse);
      return contact;
    }

    // Todo : Créer une Exception (les informations de la ville sont corrompus)
    return null;
  }

  public void creer(CreerContactCommande commande) {

    Contact contact = this.genererContact(commande);
    this.contactPortRepository.enregistrer(contact);
  }
}
