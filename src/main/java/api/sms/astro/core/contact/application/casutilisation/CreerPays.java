package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public class CreerPays {

  private final PaysPortRepository paysPortRepository;

  public CreerPays(
      PaysPortRepository paysPortRepository) {
    this.paysPortRepository = paysPortRepository;
  }

  public void creer(Pays pays) {

    List<Ville> villes = new ArrayList<>(pays.getVilles());

    pays.reinitialiserVilles();
    this.paysPortRepository.enregistrer(pays);
    Pays paysEnregistre = this.paysPortRepository
        .rechercherPaysParNom(pays.getNom());
    paysEnregistre.ajouterVilles(villes);
    villes.forEach(ville -> ville.setPays(paysEnregistre));

    this.paysPortRepository.enregistrer(paysEnregistre);
  }
}
