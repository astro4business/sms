package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.VillePortRepository;
import api.sms.astro.core.contact.domaine.Ville;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class SupprimerVille {

  private final VillePortRepository villePortRepository;

  public SupprimerVille(
      VillePortRepository villePortRepository) {
    this.villePortRepository = villePortRepository;
  }

  public void supprimer(Ville ville) {
    this.villePortRepository.supprimer(ville);
  }
}
