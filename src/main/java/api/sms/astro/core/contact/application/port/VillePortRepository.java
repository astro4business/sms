package api.sms.astro.core.contact.application.port;

import api.sms.astro.core.contact.domaine.Ville;
import java.util.List;
import java.util.Set;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public interface VillePortRepository {

  void enregistrer(Ville ville);

  void enregistrer(List<Ville> villes);

  List<Ville> enregistrerRetourner(List<Ville> villes);

  void supprimer(Ville ville);

  List<Ville> afficherListes();
}
