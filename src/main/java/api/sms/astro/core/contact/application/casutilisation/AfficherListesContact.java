package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Contact;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public class AfficherListesContact {

  private final ContactPortRepository contactPortRepository;

  public AfficherListesContact(
      ContactPortRepository contactPortRepository) {
    this.contactPortRepository = contactPortRepository;
  }

  public List<Contact> afficher() {
    return this.contactPortRepository.afficherListesContacts();
  }
}
