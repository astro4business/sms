package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.commande.ModifierContactCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Contact;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public class ModifierContact extends CreerContact {

  public ModifierContact(
      ContactPortRepository contactPortRepository) {
    super(contactPortRepository);
  }

  public Contact genererContact(ModifierContactCommande commande) {
    Contact contact = super.genererContact(commande);
    contact.setId(commande.getId());
    return contact;
  }

  public void modifier(ModifierContactCommande commande) {

    Contact contact = this.genererContact(commande);
    this.contactPortRepository.enregistrer(contact);
  }
}
