package api.sms.astro.core.contact.application.gestionnairecommande;

import api.sms.astro.core.contact.application.casutilisation.ModifierContact;
import api.sms.astro.core.contact.application.commande.ModifierContactCommande;
import api.sms.astro.core.utilisateur.GestionnaireCommande;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public class ModifierContactGestionnaire implements GestionnaireCommande<ModifierContactCommande> {

  private final ModifierContact modifierContact;

  public ModifierContactGestionnaire(
      ModifierContact modifierContact) {
    this.modifierContact = modifierContact;
  }

  @Override
  public void execute(ModifierContactCommande commande) {
    this.modifierContact.modifier(commande);
  }
}
