package api.sms.astro.core.utilisateur.domaine.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum Statut {
  AUTORISER,
  RESTREINDRE,
  SUPPRIMER
}
