package api.sms.astro.core.utilisateur.domaine;

import java.io.Serializable;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Photo implements Serializable {

  private UUID id;

  private String emplacement;
  private Byte[] photos;

  public Photo() {
  }
}
