package api.sms.astro.core.utilisateur.domaine;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueCredit implements Serializable {

  private UUID id;
  private Credit credit;
  private Utilisateur utilisateur;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public HistoriqueCredit() {}

  public HistoriqueCredit(Credit credit) {
    this.credit = credit;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Credit getCredit() {
    return credit;
  }

  public void setCredit(Credit credit) {
    this.credit = credit;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HistoriqueCredit that = (HistoriqueCredit) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, credit, utilisateur, dateCreation, dateModification);
  }

  @Override
  public String toString() {
    return "HistoriqueCredit{" +
        "id=" + id +
        ", credit=" + credit +
        ", utilisateur=" + utilisateur +
        ", dateCreation=" + dateCreation +
        ", dateModification=" + dateModification +
        '}';
  }
}
