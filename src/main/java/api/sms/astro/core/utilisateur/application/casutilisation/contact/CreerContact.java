//package api.sms.astro.core.utilisateur.application.casutilisation.contact;
//
//import api.sms.astro.core.contact.domaine.Adresse;
//import api.sms.astro.core.contact.domaine.Contact;
//import api.sms.astro.core.contact.domaine.Pays;
//import api.sms.astro.core.contact.domaine.Ville;
//import api.sms.astro.core.utilisateur.application.commande.contact.CreerContactCommande;
//import api.sms.astro.core.utilisateur.application.port.AdressePortRepository;
//import api.sms.astro.core.contact.application.port.ContactPortRepository;
//
///**
// * <p>  </p>
// *
// * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
// **/
//public class CreerContact {
//
//  private final ContactPortRepository contactPortRepository;
//
//  public CreerContact(
//      ContactPortRepository contactPortRepository) {
//    this.contactPortRepository = contactPortRepository;
//  }
//
//  public void creer(CreerContactCommande commande) {
//
//    String email = commande.getEmail();
//    String numeroTelephone = commande.getNumeroTelephone();
//    String numeroFixe = commande.getNumeroFixe();
//
//    Contact contact = new Contact(numeroTelephone);
//    contact.setEmail(email);
//    contact.setNumeroFixe(numeroFixe);
//
//    String adressePostal = commande.getAdressePostal();
//    Pays pays = commande.getPays();
//    Ville ville = commande.getVille();
//
//    Adresse adresse = new Adresse(adressePostal);
//    adresse.setPays(pays);
//    adresse.setVille(ville);
//
//    contact.setAdresse(adresse);
//
//    this.contactPortRepository.enregistrer(contact);
//  }
//}
