package api.sms.astro.core.utilisateur.domaine;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Message {

  private UUID id;

  private String objet;
  private String message;
  private Utilisateur destinataire;

  public boolean modifier = false;
  public boolean supprimer = false;

  private LocalDateTime dateOuvertureMessage;
  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public Message(String objet, String message,
      Utilisateur destinataire) {
    this.objet = objet;
    this.message = message;
    this.destinataire = destinataire;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getObjet() {
    return objet;
  }

  public void setObjet(String objet) {
    this.objet = objet;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Utilisateur getDestinataire() {
    return destinataire;
  }

  public void setDestinataire(
      Utilisateur destinataire) {
    this.destinataire = destinataire;
  }

  public LocalDateTime getDateOuvertureMessage() {
    return dateOuvertureMessage;
  }

  public void setDateOuvertureMessage(LocalDateTime dateOuvertureMessage) {
    this.dateOuvertureMessage = dateOuvertureMessage;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Message message1 = (Message) o;
    return Objects.equals(id, message1.id);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(id, objet, message, destinataire, dateOuvertureMessage, dateCreation,
            dateModification);
  }

  @Override
  public String toString() {
    return "Message{" +
        "id=" + id +
        ", objet='" + objet + '\'' +
        ", message='" + message + '\'' +
        ", destinataire=" + destinataire +
        ", dateOuvertureMessage=" + dateOuvertureMessage +
        ", dateCreation=" + dateCreation +
        ", dateModification=" + dateModification +
        '}';
  }
}
