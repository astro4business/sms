package api.sms.astro.core.utilisateur;

import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
 **/
public interface GestionnaireCommandeMultiple<A, B> {
  void execute(A cmd1, List<B> cmd2);
}
