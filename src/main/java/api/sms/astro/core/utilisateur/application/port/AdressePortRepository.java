package api.sms.astro.core.utilisateur.application.port;

import api.sms.astro.core.contact.domaine.Adresse;
import api.sms.astro.infrastructure.entite.contact.AdresseEntity;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
public interface AdressePortRepository {
  void enregistrer(Adresse adresse);
}
