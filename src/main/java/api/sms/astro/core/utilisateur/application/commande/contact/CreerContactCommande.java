//package api.sms.astro.core.utilisateur.application.commande.contact;
//
//import api.sms.astro.core.contact.domaine.Pays;
//import api.sms.astro.core.contact.domaine.Ville;
//
///**
// * <p>  </p>
// *
// * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
// **/
//public class CreerContactCommande {
//
//  private String email;
//  private String numeroTelephone;
//  private String numeroFixe;
//
//  /**
//   * Information liée à l'adresse de l'utilisateur
//   */
//  private String adressePostal;
//  private Pays pays;
//  private Ville ville;
//
//  public String getEmail() {
//    return email;
//  }
//
//  public void setEmail(String email) {
//    this.email = email;
//  }
//
//  public String getNumeroTelephone() {
//    return numeroTelephone;
//  }
//
//  public void setNumeroTelephone(String numeroTelephone) {
//    this.numeroTelephone = numeroTelephone;
//  }
//
//  public String getNumeroFixe() {
//    return numeroFixe;
//  }
//
//  public void setNumeroFixe(String numeroFixe) {
//    this.numeroFixe = numeroFixe;
//  }
//
//  public String getAdressePostal() {
//    return adressePostal;
//  }
//
//  public void setAdressePostal(String adressePostal) {
//    this.adressePostal = adressePostal;
//  }
//
//  public Pays getPays() {
//    return pays;
//  }
//
//  public void setPays(Pays pays) {
//    this.pays = pays;
//  }
//
//  public Ville getVille() {
//    return ville;
//  }
//
//  public void setVille(Ville ville) {
//    this.ville = ville;
//  }
//}
