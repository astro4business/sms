//package api.sms.astro.core.utilisateur.application.gestionnairecommande.contact;
//
//import api.sms.astro.core.contact.domaine.Contact;
//import api.sms.astro.core.utilisateur.GestionnaireCommande;
//import api.sms.astro.core.utilisateur.application.casutilisation.contact.CreerContact;
//import api.sms.astro.core.utilisateur.application.commande.contact.CreerContactCommande;
//import api.sms.astro.core.utilisateur.application.port.AdressePortRepository;
//import api.sms.astro.core.contact.application.port.ContactPortRepository;
//
///**
// * <p>  </p>
// *
// * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
// **/
//public class CreerContactGestionnaire implements
//    GestionnaireCommande<CreerContactCommande> {
//
//  private final CreerContact creerContact;
//
//  public CreerContactGestionnaire(
//      ContactPortRepository contactPortRepository) {
//    this.creerContact = new CreerContact(contactPortRepository);
//  }
//
//  @Override
//  public void execute(CreerContactCommande commande) {
//    this.creerContact.creer(commande);
//  }
//}
