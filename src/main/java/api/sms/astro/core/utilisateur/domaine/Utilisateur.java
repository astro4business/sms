package api.sms.astro.core.utilisateur.domaine;

import api.sms.astro.core.campagne.domaine.Campagne;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.prospect.domaine.Prospect;
import api.sms.astro.core.transaction.domaine.HistoriqueTransaction;
import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.exception.CreditInsuffisant;
import api.sms.astro.core.utilisateur.domaine.exception.CreditNegatif;
import api.sms.astro.core.utilisateur.domaine.objetvaleur.Statut;
import api.sms.astro.core.utilisateurentreprise.domaine.Entreprise;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public abstract class Utilisateur implements Serializable {

  public static Set<Entreprise> entreprises = new HashSet<>();
  private final Set<Contact> contact = new HashSet<>();
  private final Set<Message> messages = new HashSet<>();
  private final Set<Campagne> campagnes = new HashSet<>();
  private final Set<GroupeProspect> groupeProspects = new HashSet<>();
  private final Set<Transaction> transactions = new HashSet<>();
  private UUID id;
  private String nom;
  private Photo photo;
  private Credit credit;
  private String prenoms;
  private String motPasse;
  private String pseudonyme;
  public boolean supprimer = false;
  private Statut statut = Statut.AUTORISER;
  private Set<HistoriqueCredit> historiqueCredits;
  private Set<HistoriqueTransaction> historiqueTransactions;

  public Utilisateur() {
  }

  public Utilisateur(String nom, String motPasse) {
    this.nom = nom;
    this.motPasse = motPasse;
  }

  public Utilisateur(String nom, String prenoms, String motPasse) {
    this.nom = nom;
    this.prenoms = prenoms;
    this.motPasse = motPasse;
  }

  public static Set<Entreprise> getEntreprises() {
    return entreprises;
  }

  public static void setEntreprises(
      Set<Entreprise> entreprises) {
    Utilisateur.entreprises = entreprises;
  }

  public void envoyerMessage(String objet, String message,
      Utilisateur destinataire) {
    Message msg = new Message(objet, message, destinataire);
    destinataire.messages.add(msg);
  }

  // Todo : recuperer les 10 derniers messages
  public List<Message> lireMessagesUtilisateur(Utilisateur destinataire) {

    List<Message> messages = this.messages.stream()
        .filter(msg -> msg.getDestinataire().equals(destinataire)).collect(
            Collectors.toList());

    messages.forEach(message -> {
      if (message.getDateOuvertureMessage() == null) {
        message.setDateOuvertureMessage(LocalDateTime.now());
      }
    });

    return messages;
  }

  public void supprimerMessage(Message message) {
    this.messages.remove(message);
  }

  public void ajouterContact(String numeroTelephone) {
    this.contact.add(new Contact(numeroTelephone));
  }

  public void supprimerContact(Contact contact) {
    this.contact.remove(contact);
  }

  public void crediterCompte(BigDecimal credit) {
    if (credit.compareTo(BigDecimal.ZERO) < 0) {
      throw new CreditNegatif();
    }

    HistoriqueCredit historiqueCredit = new HistoriqueCredit(this.credit);
    this.historiqueCredits.add(historiqueCredit);

    this.credit.setCredit(this.credit.getCredit().add(credit));
  }

  public void debiterCompte(BigDecimal credit) {
    if (credit.compareTo(BigDecimal.ZERO) < 0) {
      throw new CreditNegatif();
    }

    int creditActuel = this.credit.getCredit().intValue();
    if (creditActuel <= 0 || creditActuel < credit.intValue()) {
      throw new CreditInsuffisant();
    }

    HistoriqueCredit historiqueCredit = new HistoriqueCredit(this.credit);
    this.historiqueCredits.add(historiqueCredit);

    this.credit.setCredit(this.credit.getCredit().subtract(credit));
  }

  public void historiserTransaction(HistoriqueTransaction historiqueTransaction) {
    historiqueTransaction.setUtilisateur(this);
    this.historiqueTransactions.add(historiqueTransaction);
  }

  public void creerCampagne(Campagne campagne) {
    this.campagnes.add(campagne);
  }

  public void supprimerCampagne(Campagne campagne) {
    this.campagnes.remove(campagne);
  }

  public void ajouterGroupeProspect(GroupeProspect groupeProspect) {
    this.groupeProspects.add(groupeProspect);
  }

  public void supprimerGroupeProspect(GroupeProspect groupeProspect) {
    this.groupeProspects.remove(groupeProspect);
  }

  public void ajouterProspect(GroupeProspect groupeProspect, Prospect prospect) {
    Optional<GroupeProspect> optionalGroupeProspect = this.groupeProspects.stream()
        .filter(grpeProspect -> grpeProspect.equals(groupeProspect)).findFirst();
    optionalGroupeProspect
        .ifPresent(grpeProspect -> grpeProspect.ajouterProspect(prospect));
  }

  public void supprimerProspect(GroupeProspect groupeProspect, Prospect prospect) {
    Optional<GroupeProspect> optionalGroupeProspect = this.groupeProspects.stream()
        .filter(grpeProspect -> grpeProspect.equals(groupeProspect)).findFirst();
    optionalGroupeProspect
        .ifPresent(grpeProspect -> grpeProspect.supprimerProspect(prospect));
  }

  public void ajouterListeProspect(GroupeProspect groupeProspect,
      Set<Prospect> prospects) {
    Optional<GroupeProspect> optionalGroupeProspect = this.groupeProspects.stream()
        .filter(grpeProspect -> grpeProspect.equals(groupeProspect)).findFirst();
    optionalGroupeProspect
        .ifPresent(grpeProspect -> grpeProspect.ajouterListeProspect(prospects));
  }

  public void supprimerListeProspect(GroupeProspect groupeProspect,
      Set<Prospect> prospects) {
    Optional<GroupeProspect> optionalGroupeProspect = this.groupeProspects.stream()
        .filter(grpeProspect -> grpeProspect.equals(groupeProspect)).findFirst();
    optionalGroupeProspect
        .ifPresent(grpeProspect -> grpeProspect.supprimerListeProspect(prospects));
  }

  public void ajouterCampagne(Campagne campagne) {
    this.campagnes.add(campagne);
    campagne.setUtilisateur(this);
  }

  public void ajouterEntreprise(Entreprise entreprise) {
    entreprises.add(entreprise);
  }

  public void effectuerTransaction(Transaction transaction) {
    this.transactions.add(transaction);
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenoms() {
    return prenoms;
  }

  public void setPrenoms(String prenoms) {
    this.prenoms = prenoms;
  }

  public String getPseudonyme() {
    return pseudonyme;
  }

  public void setPseudonyme(String pseudonyme) {
    this.pseudonyme = pseudonyme;
  }

  public String getMotPasse() {
    return motPasse;
  }

  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  public Photo getPhoto() {
    return photo;
  }

  public void setPhoto(Photo photo) {
    this.photo = photo;
  }

  public Credit getCredit() {
    return credit;
  }

  public void setCredit(Credit credit) {
    this.credit = credit;
  }

  public Set<Contact> getContact() {
    return contact;
  }

  public Set<Message> getMessages() {
    return messages;
  }

  public Set<Campagne> getCampagnes() {
    return campagnes;
  }

  public Set<GroupeProspect> getGroupeProspects() {
    return groupeProspects;
  }

  public Statut getStatut() {
    return statut;
  }

  public void setStatut(Statut statut) {
    this.statut = statut;
  }

  public Set<HistoriqueCredit> getHistoriqueCredits() {
    return historiqueCredits;
  }

  public void setHistoriqueCredits(
      Set<HistoriqueCredit> historiqueCredits) {
    this.historiqueCredits = historiqueCredits;
  }

  public Set<HistoriqueTransaction> getHistoriqueTransactions() {
    return historiqueTransactions;
  }

  public void setHistoriqueTransactions(
      Set<HistoriqueTransaction> historiqueTransactions) {
    this.historiqueTransactions = historiqueTransactions;
  }
}
