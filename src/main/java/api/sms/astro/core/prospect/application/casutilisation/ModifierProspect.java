package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.contact.application.casutilisation.ModifierContact;
import api.sms.astro.core.contact.application.commande.ModifierContactCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.application.commande.ModifierProspectCommande;
import api.sms.astro.core.prospect.application.port.ProspectPortRepository;
import api.sms.astro.core.prospect.domaine.Prospect;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class ModifierProspect extends CreerProspect {

  private final ProspectPortRepository prospectPortRepository;
  private final ModifierContact modifierContact;

  public ModifierProspect(
      ProspectPortRepository prospectPortRepository,
      ContactPortRepository contactPortRepository) {
    super(prospectPortRepository, contactPortRepository);
    this.prospectPortRepository = prospectPortRepository;
    this.modifierContact = new ModifierContact(contactPortRepository);
  }

  public void modifier(ModifierProspectCommande commandeProspect,
      List<ModifierContactCommande> commandeContact) {

    Prospect prospectClient = this.prospectPortRepository
        .rechercherProspectParId(commandeProspect.getId());

    if (prospectClient != null) {

      Prospect prospect = this.genererProspect(commandeProspect);
      prospect.setId(commandeProspect.getId());

      List<Contact> contacts = commandeContact.stream()
          .map(this.modifierContact::genererContact)
          .collect(
              Collectors.toList());

      prospect.setContacts(contacts);

      this.prospectPortRepository.enregistrer(prospect);
    } else {
      throw new RuntimeException();
      // Todo : Generer Exception (l'utilisateur ayant l'id n'est pas disponible dans la bd)
    }
  }
}
