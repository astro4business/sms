package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.prospect.application.port.GroupeProspectPortRepository;
import api.sms.astro.core.prospect.domaine.GroupeProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 26/08/2020
 **/
public class SupprimerGroupeProspect {

  private final GroupeProspectPortRepository groupeProspectPortRepository;

  public SupprimerGroupeProspect(
      GroupeProspectPortRepository groupeProspectPortRepository) {
    this.groupeProspectPortRepository = groupeProspectPortRepository;
  }

  public void supprimer(GroupeProspect groupeProspect) {
    this.groupeProspectPortRepository.supprimer(groupeProspect);
  }
}
