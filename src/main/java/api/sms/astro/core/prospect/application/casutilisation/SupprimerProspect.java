package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.prospect.application.port.ProspectPortRepository;
import api.sms.astro.core.prospect.domaine.Prospect;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class SupprimerProspect {

  private final ProspectPortRepository prospectPortRepository;

  public SupprimerProspect(
      ProspectPortRepository prospectPortRepository) {
    this.prospectPortRepository = prospectPortRepository;
  }

  public void supprimer(Prospect prospect) {
    this.prospectPortRepository.supprimer(prospect);
  }

  public void supprimer(List<Prospect> prospectList) {
    this.prospectPortRepository.supprimerListProspect(prospectList);
  }
}
