package api.sms.astro.core.prospect.application.commande;

import api.sms.astro.core.contact.domaine.Contact;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 26/08/2020
 **/
public class ModifierProspectCommande extends CreerProspectCommande {

  @NotBlank
  private UUID id;

  @NotBlank
  private List<Contact> contacts;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public List<Contact> getContacts() {
    return contacts;
  }

  public void setContacts(
      List<Contact> contacts) {
    this.contacts = contacts;
  }
}
