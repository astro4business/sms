package api.sms.astro.core.prospect.domaine;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class EntrepriseProspect implements Serializable {

  private UUID id;
  private String nom;
  private List<Prospect> prospects;
  private boolean supprimer = false;

  public EntrepriseProspect(String nom) {
    this.nom = nom;
  }

  public void ajouterProspect(Prospect prospect) {
    this.prospects.add(prospect);
  }

  public void ajouterProspects(List<Prospect> prospects) {
    this.prospects.addAll(prospects);
  }

  public void supprimerProspect(Prospect prospect) {
    this.prospects.remove(prospect);
  }

  public void supprimerProspects(List<Prospect> prospects) {
    this.prospects.removeAll(prospects);
  }

  public List<Prospect> getProspects() {
    return prospects;
  }

  public void setProspects(
      List<Prospect> prospects) {
    this.prospects = prospects;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EntrepriseProspect that = (EntrepriseProspect) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom);
  }

  @Override
  public String toString() {
    return "EntrepriseProspect{" +
        "id=" + id +
        ", nom='" + nom + '\'' +
        '}';
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }
}
