package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.contact.application.casutilisation.CreerContact;
import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.application.commande.CreerProspectCommande;
import api.sms.astro.core.prospect.application.port.ProspectPortRepository;
import api.sms.astro.core.prospect.domaine.EntrepriseProspect;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.prospect.domaine.Prospect;
import api.sms.astro.core.prospect.domaine.objetvaleur.Genre;
import api.sms.astro.core.prospect.domaine.objetvaleur.StatutProspect;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class CreerProspect {

  private final ProspectPortRepository prospectPortRepository;
  private final ContactPortRepository contactPortRepository;

  public CreerProspect(
      ProspectPortRepository prospectPortRepository,
      ContactPortRepository contactPortRepository) {
    this.prospectPortRepository = prospectPortRepository;
    this.contactPortRepository = contactPortRepository;
  }

  protected Prospect genererProspect(CreerProspectCommande commande) {
    String nom = commande.getNom();
    String prenoms = commande.getPrenoms();

    LocalDate dateNaissance = commande.getDateNaissance();
    EntrepriseProspect entrepriseProspect = commande.getEntrepriseProspect();
    Genre genre = commande.getGenre();
    StatutProspect statutProspect = commande.getStatutProspect();
    List<GroupeProspect> groupeProspects = commande.getGroupeProspects();

    Prospect prospect = new Prospect(nom, prenoms);

    prospect.setDateNaissance(dateNaissance);
    prospect.setEntrepriseProspect(entrepriseProspect);
    prospect.setGenre(genre);
    prospect.setStatutProspect(statutProspect);
    prospect.setGroupeProspects(groupeProspects);

    return prospect;
  }

  public void creer(CreerProspectCommande commande,
      List<CreerContactCommande> commandeContact) {

    CreerContact creerContact = new CreerContact(this.contactPortRepository);

    List<Contact> contacts = commandeContact.stream()
        .map(creerContact::genererContact).collect(
            Collectors.toList());

    Prospect prospect = this.genererProspect(commande);
    prospect.setContacts(contacts);

    this.prospectPortRepository.enregistrer(prospect);
  }
}
