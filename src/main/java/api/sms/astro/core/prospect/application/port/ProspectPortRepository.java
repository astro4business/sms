package api.sms.astro.core.prospect.application.port;

import api.sms.astro.core.prospect.domaine.EntrepriseProspect;
import api.sms.astro.core.prospect.domaine.Prospect;
import java.util.List;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public interface ProspectPortRepository {

  void enregistrer(EntrepriseProspect entrepriseProspect);

  void enregistrer(Prospect prospect);

  void supprimer(EntrepriseProspect entrepriseProspect);

  Prospect rechercherProspectParId(UUID id);

  void supprimer(Prospect prospect);

  void supprimerListProspect(List<Prospect> prospectList);
}
