package api.sms.astro.core.prospect.application.port;

import api.sms.astro.core.prospect.domaine.GroupeProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 26/08/2020
 **/
public interface GroupeProspectPortRepository {

  void enregistrer(GroupeProspect groupeProspect);

  void supprimer(GroupeProspect groupeProspect);
}
