package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.prospect.application.port.ProspectPortRepository;
import api.sms.astro.core.prospect.domaine.EntrepriseProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class SupprimerEntrepriseProspect {

  private final ProspectPortRepository prospectPortRepository;

  public SupprimerEntrepriseProspect(
      ProspectPortRepository prospectPortRepository) {
    this.prospectPortRepository = prospectPortRepository;
  }

  public void supprimer(EntrepriseProspect entrepriseProspect) {
    this.prospectPortRepository.supprimer(entrepriseProspect);
  }
}
