package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.prospect.application.port.GroupeProspectPortRepository;
import api.sms.astro.core.prospect.domaine.GroupeProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 26/08/2020
 **/
public class EnregistrerGroupeProspect {

  private final GroupeProspectPortRepository groupeProspectPortRepository;

  public EnregistrerGroupeProspect(
      GroupeProspectPortRepository groupeProspectPortRepository) {
    this.groupeProspectPortRepository = groupeProspectPortRepository;
  }

  public void enregistrer(GroupeProspect groupeProspect) {
    // Todo : Système de verification en cas de modification du groupe de prospect.
    this.groupeProspectPortRepository.enregistrer(groupeProspect);
  }
}
