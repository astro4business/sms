package api.sms.astro.core.prospect.application.port;

import api.sms.astro.core.prospect.domaine.EntrepriseProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 26/08/2020
 **/
public interface EntrepriseProspectPortRepository {

  void enregistrer(EntrepriseProspect entrepriseProspect);
}
