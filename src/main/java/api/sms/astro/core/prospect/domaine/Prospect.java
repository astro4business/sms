package api.sms.astro.core.prospect.domaine;

import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.domaine.objetvaleur.Genre;
import api.sms.astro.core.prospect.domaine.objetvaleur.StatutProspect;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class Prospect implements Serializable {

  private UUID id;

  private String nom;
  private String prenoms;

  private LocalDate dateNaissance;

  private Genre genre;
  private StatutProspect statutProspect = StatutProspect.AUCUN;
  private EntrepriseProspect entrepriseProspect;

  private List<GroupeProspect> groupeProspects = new ArrayList<>();

  private List<Contact> contacts = new ArrayList<>();

  private boolean supprimer = false;

  public Prospect() {
    super();
  }

  public Prospect(String nom, String prenoms) {
    this.nom = nom;
    this.prenoms = prenoms;
  }

  public Prospect(String nom, String prenoms, List<Contact> contacts) {
    this.nom = nom;
    this.prenoms = prenoms;
    this.contacts.addAll(contacts);
  }

  void ajouterContacts(List<Contact> contacts) {
    this.contacts.addAll(contacts);
  }

  void supprimerContacts(List<Contact> contacts) {
    this.contacts.forEach(contact -> contact.setSupprimer(true));
  }

  public void ajouterGroupeProspects(List<GroupeProspect> groupeProspects) {
    this.groupeProspects.addAll(groupeProspects);
    groupeProspects.forEach(groupeProspect -> groupeProspect.ajouterProspect(this));
  }

  public void supprimerGroupeProspects(List<GroupeProspect> groupeProspects) {
    this.groupeProspects.forEach(groupeProspect -> groupeProspect.setSupprimer(true));
//    groupeProspects.forEach(groupeProspect -> groupeProspect.supprimerProspect(this));
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenoms() {
    return prenoms;
  }

  public void setPrenoms(String prenoms) {
    this.prenoms = prenoms;
  }

  public LocalDate getDateNaissance() {
    return dateNaissance;
  }

  public void setDateNaissance(LocalDate dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  public Genre getGenre() {
    return genre;
  }

  public void setGenre(Genre genre) {
    this.genre = genre;
  }

  public StatutProspect getStatutProspect() {
    return statutProspect;
  }

  public void setStatutProspect(
      StatutProspect statutProspect) {
    this.statutProspect = statutProspect;
  }

  public EntrepriseProspect getEntrepriseProspect() {
    return entrepriseProspect;
  }

  public void setEntrepriseProspect(
      EntrepriseProspect entrepriseProspect) {
    this.entrepriseProspect = entrepriseProspect;
  }

  public List<GroupeProspect> getGroupeProspects() {
    return groupeProspects;
  }

  public void setGroupeProspects(
      List<GroupeProspect> groupeProspects) {
    this.groupeProspects = groupeProspects;
  }

  public List<Contact> getContacts() {
    return contacts;
  }

  public void setContacts(List<Contact> contacts) {
    this.contacts = contacts;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Prospect prospect = (Prospect) o;
    return supprimer == prospect.supprimer &&
        Objects.equals(id, prospect.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, prenoms, dateNaissance, genre, statutProspect,
        entrepriseProspect, groupeProspects, contacts, supprimer);
  }
}
