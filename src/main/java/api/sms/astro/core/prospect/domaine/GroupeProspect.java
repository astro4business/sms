package api.sms.astro.core.prospect.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class GroupeProspect implements Serializable {

  private UUID id;
  private Set<Prospect> prospects;
  private Utilisateur utilisateur;

  private boolean supprimer = false;

  public GroupeProspect(
      Set<Prospect> prospects) {
    this.prospects = prospects;
  }

  public void ajouterProspect(Prospect prospect) {
    prospects.add(prospect);
  }

  public void ajouterListeProspect(Set<Prospect> prospects) {
    this.prospects.addAll(prospects);
  }

  public void supprimerListeProspect(Set<Prospect> prospects) {
    this.prospects.removeAll(prospects);
  }

  public void supprimerProspect(Prospect prospect) {
    prospects.remove(prospect);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupeProspect that = (GroupeProspect) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, prospects);
  }

  @Override
  public String toString() {
    return "GroupeProspect{" +
        "id=" + id +
        ", prospects=" + prospects +
        '}';
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Set<Prospect> getProspects() {
    return prospects;
  }

  public void setProspects(
      Set<Prospect> prospects) {
    this.prospects = prospects;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }
}
