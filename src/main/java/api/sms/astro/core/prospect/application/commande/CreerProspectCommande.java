package api.sms.astro.core.prospect.application.commande;

import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.domaine.EntrepriseProspect;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.prospect.domaine.objetvaleur.Genre;
import api.sms.astro.core.prospect.domaine.objetvaleur.StatutProspect;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import jdk.vm.ci.meta.Local;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class CreerProspectCommande {

  @NotEmpty
  private String nom;

  @NotEmpty
  private String prenoms;

  @NotNull
  private LocalDate dateNaissance;

  @NotBlank
  private Genre genre;

  @NotBlank
  private StatutProspect statutProspect = StatutProspect.AUCUN;

  @NotNull
  private EntrepriseProspect entrepriseProspect;

  private List<GroupeProspect> groupeProspects = new ArrayList<>();

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenoms() {
    return prenoms;
  }

  public void setPrenoms(String prenoms) {
    this.prenoms = prenoms;
  }

  public LocalDate getDateNaissance() {
    return dateNaissance;
  }

  public void setDateNaissance(LocalDate dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  public Genre getGenre() {
    return genre;
  }

  public void setGenre(Genre genre) {
    this.genre = genre;
  }

  public StatutProspect getStatutProspect() {
    return statutProspect;
  }

  public void setStatutProspect(
      StatutProspect statutProspect) {
    this.statutProspect = statutProspect;
  }

  public EntrepriseProspect getEntrepriseProspect() {
    return entrepriseProspect;
  }

  public void setEntrepriseProspect(
      EntrepriseProspect entrepriseProspect) {
    this.entrepriseProspect = entrepriseProspect;
  }

  public List<GroupeProspect> getGroupeProspects() {
    return groupeProspects;
  }

  public void setGroupeProspects(
      List<GroupeProspect> groupeProspects) {
    this.groupeProspects = groupeProspects;
  }
}
