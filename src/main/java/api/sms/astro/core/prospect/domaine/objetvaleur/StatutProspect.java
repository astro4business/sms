package api.sms.astro.core.prospect.domaine.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum StatutProspect {
  DIAMAND,
  OR,
  ARGENT,
  BRONZE,
  AUCUN
}
