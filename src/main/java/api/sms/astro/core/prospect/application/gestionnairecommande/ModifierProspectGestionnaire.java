package api.sms.astro.core.prospect.application.gestionnairecommande;

import api.sms.astro.core.contact.application.commande.ModifierContactCommande;
import api.sms.astro.core.prospect.application.casutilisation.ModifierProspect;
import api.sms.astro.core.prospect.application.commande.ModifierProspectCommande;
import api.sms.astro.core.utilisateur.GestionnaireCommandeMultiple;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class ModifierProspectGestionnaire implements GestionnaireCommandeMultiple<ModifierProspectCommande, ModifierContactCommande> {

  private final ModifierProspect modifierProspect;

  public ModifierProspectGestionnaire(
      ModifierProspect modifierProspect) {
    this.modifierProspect = modifierProspect;
  }

  @Override
  public void execute(ModifierProspectCommande commandeProspect, List<ModifierContactCommande> commandeContact) {
    this.modifierProspect.modifier(commandeProspect, commandeContact);
  }
}
