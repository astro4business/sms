package api.sms.astro.core.prospect.application.casutilisation;

import api.sms.astro.core.prospect.application.port.EntrepriseProspectPortRepository;
import api.sms.astro.core.prospect.domaine.EntrepriseProspect;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class EnregistrerEntrepriseProspect {

  private final EntrepriseProspectPortRepository entrepriseProspectPortRepository;

  public EnregistrerEntrepriseProspect(
      EntrepriseProspectPortRepository entrepriseProspectPortRepository) {
    this.entrepriseProspectPortRepository = entrepriseProspectPortRepository;
  }

  public void creer(EntrepriseProspect entrepriseProspect) {
    // Todo : Système de verification d'ajout en cas d'existance de l'id alors apporter des modifications.
    this.entrepriseProspectPortRepository.enregistrer(entrepriseProspect);
  }
}
