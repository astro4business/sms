package api.sms.astro.core.prospect.application.gestionnairecommande;

import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.prospect.application.casutilisation.CreerProspect;
import api.sms.astro.core.prospect.application.commande.CreerProspectCommande;
import api.sms.astro.core.prospect.application.port.ProspectPortRepository;
import api.sms.astro.core.utilisateur.GestionnaireCommandeMultiple;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
public class CreerProspectGestionnaire implements
    GestionnaireCommandeMultiple<CreerProspectCommande, CreerContactCommande> {

  private final CreerProspect creerProspect;

  public CreerProspectGestionnaire(
      ProspectPortRepository prospectPortRepository,
      ContactPortRepository contactPortRepository) {
    this.creerProspect = new CreerProspect(prospectPortRepository,
        contactPortRepository);
  }

  @Override
  public void execute(CreerProspectCommande commandeProspect,
      List<CreerContactCommande> commandeContact) {
    this.creerProspect.creer(commandeProspect, commandeContact);
  }

}
