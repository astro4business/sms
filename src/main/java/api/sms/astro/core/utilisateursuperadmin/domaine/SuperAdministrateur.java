package api.sms.astro.core.utilisateursuperadmin.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.core.utilisateur.domaine.objetvaleur.Statut;
import api.sms.astro.core.utilisateuradministrateur.domaine.Administrateur;
import java.math.BigDecimal;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class SuperAdministrateur extends Administrateur {

  public SuperAdministrateur(String nom, String prenoms, String motPasse,
      String secondMotPasse) {
    super(nom, prenoms, motPasse, secondMotPasse);
  }

  public void modifierStatut(Utilisateur utilisateur, Statut statut) {
    utilisateur.setStatut(statut);
  }

  public void crediterUtilisateur(Utilisateur utilisateur, BigDecimal credit) {
    utilisateur.crediterCompte(credit);
  }

  public void debiterUtilisateur(Utilisateur utilisateur, BigDecimal credit) {
    utilisateur.debiterCompte(credit);
  }
}
