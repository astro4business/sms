package api.sms.astro.infrastructure.mapper;

import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import api.sms.astro.infrastructure.entite.contact.PaysEntity;
import api.sms.astro.infrastructure.entite.contact.VilleEntity;
import java.util.List;
import java.util.Set;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Mapper(componentModel = "spring")
public interface PaysMapper {

  Pays entityToPays(PaysEntity paysEntity);
  PaysEntity paysToEntity(Pays pays);

  @Mapping(target = "pays", ignore = true)
  Ville entityToVille(VilleEntity villeEntity);
  @Mapping(target = "pays.villes", ignore = true)
  VilleEntity villeToEntity(Ville ville);

  List<Pays> entitiesToPays(List<PaysEntity> paysEntities);
  List<PaysEntity> paysToEntities(Set<Pays> pays);
}
