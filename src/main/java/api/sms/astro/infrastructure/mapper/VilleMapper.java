package api.sms.astro.infrastructure.mapper;

import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import api.sms.astro.infrastructure.entite.contact.PaysEntity;
import api.sms.astro.infrastructure.entite.contact.VilleEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Mapper(componentModel = "spring")
public interface VilleMapper {

  Ville entityToVille(VilleEntity villeEntity);

  VilleEntity villeToEntity(Ville ville);

  @Mapping(target = "villes", ignore = true)
  Pays entityToPays(PaysEntity paysEntity);

  @Mapping(target = "villes", ignore = true)
  PaysEntity paysToEntity(Pays pays);

  List<Ville> entitiesToVilles(List<VilleEntity> villeEntities);

  List<VilleEntity> villesToEntities(List<Ville> villes);
}
