package api.sms.astro.infrastructure.mapper;

import api.sms.astro.core.contact.domaine.Adresse;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import api.sms.astro.infrastructure.entite.contact.AdresseEntity;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import api.sms.astro.infrastructure.entite.contact.PaysEntity;
import api.sms.astro.infrastructure.entite.contact.VilleEntity;
import java.util.List;
import org.mapstruct.Mapper;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Mapper(componentModel = "spring")
public interface ContactMapper {

  Contact entityToContact(ContactEntity contactEntity);

  ContactEntity contactToEntity(Contact contact);

  Adresse entityToAdresse(AdresseEntity adresseEntity);

  AdresseEntity adresseToEntity(Adresse adresse);

  Pays entityToPays(PaysEntity paysEntity);

  PaysEntity paysToEntity(Pays pays);

  Ville entityToVille(VilleEntity villeEntity);

  VilleEntity villeToEntity(Ville ville);

  List<Contact> entitiesToContacts(List<ContactEntity> contactEntities);

  List<ContactEntity> contactsToEntities(List<Contact> contacts);
}
