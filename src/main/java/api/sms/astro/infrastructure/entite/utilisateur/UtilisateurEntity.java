package api.sms.astro.infrastructure.entite.utilisateur;

import api.sms.astro.core.campagne.domaine.Campagne;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.prospect.domaine.Prospect;
import api.sms.astro.core.transaction.domaine.HistoriqueTransaction;
import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.Credit;
import api.sms.astro.core.utilisateur.domaine.HistoriqueCredit;
import api.sms.astro.core.utilisateur.domaine.Message;
import api.sms.astro.core.utilisateur.domaine.Photo;
import api.sms.astro.core.utilisateur.domaine.exception.CreditInsuffisant;
import api.sms.astro.core.utilisateur.domaine.exception.CreditNegatif;
import api.sms.astro.core.utilisateur.domaine.objetvaleur.Statut;
import api.sms.astro.core.utilisateurentreprise.domaine.Entreprise;
import api.sms.astro.infrastructure.entite.campagne.CampagneEntity;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import api.sms.astro.infrastructure.entite.prospect.GroupeProspectEntity;
import api.sms.astro.infrastructure.entite.transaction.HistoriqueTransactionEntity;
import api.sms.astro.infrastructure.entite.transaction.TransactionEntity;
import api.sms.astro.infrastructure.entite.utilisateur.objetvaleur.StatutEntity;
import api.sms.astro.infrastructure.entite.utilisateurentreprise.EntrepriseEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public abstract class UtilisateurEntity implements Serializable {

  public static Set<EntrepriseEntity> entreprises = new HashSet<>();
  private final Set<ContactEntity> contact = new HashSet<>();
  private final Set<MessageEntity> messages = new HashSet<>();
  private final Set<CampagneEntity> campagnes = new HashSet<>();
  private final Set<GroupeProspectEntity> groupeProspects = new HashSet<>();
  private final Set<TransactionEntity> transactions = new HashSet<>();
  public boolean supprimer = false;
  private UUID id;
  private String nom;
  private PhotoEntity photo;
  private CreditEntity credit;
  private String prenoms;
  private String motPasse;
  private String pseudonyme;
  private StatutEntity statut = StatutEntity.AUTORISER;
  private Set<HistoriqueCreditEntity> historiqueCredits;
  private Set<HistoriqueTransactionEntity> historiqueTransactions;

  public UtilisateurEntity() {
  }

  public UtilisateurEntity(String nom, String motPasse) {
    this.nom = nom;
    this.motPasse = motPasse;
  }

  public UtilisateurEntity(String nom, String prenoms, String motPasse) {
    this.nom = nom;
    this.prenoms = prenoms;
    this.motPasse = motPasse;
  }
}
