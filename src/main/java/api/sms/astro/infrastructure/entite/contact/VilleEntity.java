package api.sms.astro.infrastructure.entite.contact;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * <p> Objet valeur permettant l'enregistrement d'une ville </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "VILLE")
public class VilleEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column
  private UUID id;

  @Column(nullable = false)
  private String nom;

  @Column(unique = true)
  private String coordonnee;

  @Column(unique = true, nullable = false)
  private String codeISO;

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "pays_entity_id", nullable = false)
  private PaysEntity pays;

  @CreationTimestamp
  private LocalDateTime dateCreation = LocalDateTime.now();

  @UpdateTimestamp
  private LocalDateTime dateModification = LocalDateTime.now();

  public VilleEntity() {
    super();
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  @PrePersist
  public void actualiseDateCreation() {
    this.dateCreation = LocalDateTime.now();
  }

  @PreUpdate
  public void actualiseDateModification() {
    this.dateModification = LocalDateTime.now();
  }

  public PaysEntity getPays() {
    return pays;
  }

  public void setPays(PaysEntity pays) {
    this.pays = pays;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCoordonnee() {
    return coordonnee;
  }

  public void setCoordonnee(String coordonnee) {
    this.coordonnee = coordonnee;
  }

  public String getCodeISO() {
    return codeISO;
  }

  public void setCodeISO(String codeISO) {
    this.codeISO = codeISO;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VilleEntity that = (VilleEntity) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, coordonnee, codeISO, supprimer);
  }
}
