package api.sms.astro.infrastructure.entite.campagne.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum ModeEnvoieEntity {
  INSTANTANEE,
  DIFFERE
}
