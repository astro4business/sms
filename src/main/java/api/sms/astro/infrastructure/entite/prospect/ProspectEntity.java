package api.sms.astro.infrastructure.entite.prospect;

import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import api.sms.astro.infrastructure.entite.prospect.objetvaleur.GenreEntity;
import api.sms.astro.infrastructure.entite.prospect.objetvaleur.StatutProspectEntity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class ProspectEntity implements Serializable {

  private UUID id;

  private String nom;
  private String prenoms;

  private LocalDateTime dateNaissance;

  private GenreEntity genre;
  private StatutProspectEntity statutProspect = StatutProspectEntity.BRONZE;
  private EntrepriseProspectEntity entrepriseProspect;

  private Set<GroupeProspectEntity> groupeProspect = new HashSet<>();

  private ContactEntity contact;

  private boolean supprimer = false;

  public ProspectEntity() {
  }

  public ProspectEntity(String nom, String prenoms,
      ContactEntity contact) {
    this.nom = nom;
    this.prenoms = prenoms;
    this.contact = contact;
  }
}
