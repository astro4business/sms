package api.sms.astro.infrastructure.entite.utilisateur;

import java.io.Serializable;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class PhotoEntity implements Serializable {

  private UUID id;

  private String emplacement;
  private Byte[] photos;

  private boolean supprimer = false;
}
