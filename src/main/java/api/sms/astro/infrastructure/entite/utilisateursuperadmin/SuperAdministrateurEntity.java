package api.sms.astro.infrastructure.entite.utilisateursuperadmin;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.core.utilisateur.domaine.objetvaleur.Statut;
import api.sms.astro.core.utilisateuradministrateur.domaine.Administrateur;
import java.math.BigDecimal;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class SuperAdministrateurEntity extends Administrateur {

  public SuperAdministrateurEntity(String nom, String prenoms, String motPasse,
      String secondMotPasse) {
    super(nom, prenoms, motPasse, secondMotPasse);
  }
}
