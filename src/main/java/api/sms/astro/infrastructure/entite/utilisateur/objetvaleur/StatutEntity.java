package api.sms.astro.infrastructure.entite.utilisateur.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum StatutEntity {
  AUTORISER,
  RESTREINDRE,
  SUPPRIMER
}
