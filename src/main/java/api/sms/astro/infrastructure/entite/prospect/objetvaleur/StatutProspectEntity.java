package api.sms.astro.infrastructure.entite.prospect.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum StatutProspectEntity {
  DIAMAND,
  OR,
  ARGENT,
  BRONZE
}
