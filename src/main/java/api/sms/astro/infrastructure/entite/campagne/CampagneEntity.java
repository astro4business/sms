package api.sms.astro.infrastructure.entite.campagne;

import api.sms.astro.core.campagne.domaine.MessageCampagne;
import api.sms.astro.core.campagne.domaine.objetvaleur.Audience;
import api.sms.astro.core.campagne.domaine.objetvaleur.ModeEnvoie;
import api.sms.astro.core.campagne.domaine.objetvaleur.PeriodeCampagne;
import api.sms.astro.core.prospect.domaine.GroupeProspect;
import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.campagne.objetvaleur.AudienceEntity;
import api.sms.astro.infrastructure.entite.campagne.objetvaleur.ModeEnvoieEntity;
import api.sms.astro.infrastructure.entite.prospect.GroupeProspectEntity;
import api.sms.astro.infrastructure.entite.transaction.TransactionEntity;
import api.sms.astro.infrastructure.entite.utilisateur.UtilisateurEntity;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class CampagneEntity {

  private UUID id;
  private String intitule;
  private String numeroCampagne;

  private UtilisateurEntity utilisateur;

  private PeriodeCampagne periodeCampagne;
  private AudienceEntity audience;
  private Set<GroupeProspectEntity> groupeProspects;

  private MessageCampagneEntity messageCampagne;
  private final ModeEnvoieEntity modeEnvoie = ModeEnvoieEntity.INSTANTANEE;

  private TransactionEntity transaction;

  private boolean supprimer = false;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
