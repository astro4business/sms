package api.sms.astro.infrastructure.entite.utilisateur;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class CreditEntity implements Serializable {

  private UUID id;
  private BigDecimal credit;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public CreditEntity() {
    this.credit = new BigDecimal("0");
  }

  public CreditEntity(BigDecimal credit) {
    this.credit = credit;
  }
}
