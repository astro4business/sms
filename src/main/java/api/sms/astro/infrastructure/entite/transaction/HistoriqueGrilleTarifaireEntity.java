package api.sms.astro.infrastructure.entite.transaction;

import api.sms.astro.core.transaction.domaine.GrilleTarifaire;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueGrilleTarifaireEntity {

  private UUID id;

  private LocalDateTime dateHistorisation;

  private Set<GrilleTarifaireEntity> grilleTarifaires;
}
