package api.sms.astro.infrastructure.entite.contact;

import api.sms.astro.core.contact.domaine.Adresse;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.utilisateur.UtilisateurEntity;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> Entité domaine Contact </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "CONTACT")
public class ContactEntity implements Serializable {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private String email;

  @Column
  private String numeroTelephone;

  @Column
  private String numeroFixe;

  @Embedded
  private AdresseEntity adresse;

//  @OneToMany
//  private UtilisateurEntity utilisateur;

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;

  public ContactEntity() {
    super();
  }

  public ContactEntity(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNumeroTelephone() {
    return numeroTelephone;
  }

  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public String getNumeroFixe() {
    return numeroFixe;
  }

  public void setNumeroFixe(String numeroFixe) {
    this.numeroFixe = numeroFixe;
  }

  public AdresseEntity getAdresse() {
    return adresse;
  }

  public void setAdresse(AdresseEntity adresse) {
    this.adresse = adresse;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContactEntity that = (ContactEntity) o;
    return supprimer == that.supprimer &&
        Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, numeroTelephone, numeroFixe, adresse, supprimer);
  }
}
