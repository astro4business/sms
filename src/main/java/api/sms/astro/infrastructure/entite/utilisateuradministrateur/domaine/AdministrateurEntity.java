package api.sms.astro.infrastructure.entite.utilisateuradministrateur.domaine;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.core.utilisateurentreprise.domaine.Entreprise;
import api.sms.astro.core.utilisateurentreprise.domaine.MembreEntreprise;
import api.sms.astro.core.utilisateurentreprise.domaine.objetvaleur.StatutClient;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class AdministrateurEntity extends Utilisateur {

  private String secondMotPasse;

  public AdministrateurEntity(String nom, String prenoms, String motPasse,
      String secondMotPasse) {
    super(nom, prenoms, motPasse);
    this.secondMotPasse = secondMotPasse;
  }
}
