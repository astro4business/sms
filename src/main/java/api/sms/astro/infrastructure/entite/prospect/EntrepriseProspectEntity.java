package api.sms.astro.infrastructure.entite.prospect;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class EntrepriseProspectEntity implements Serializable {

  private UUID id;
  private String nom;

  public EntrepriseProspectEntity(String nom) {
    this.nom = nom;
  }
}
