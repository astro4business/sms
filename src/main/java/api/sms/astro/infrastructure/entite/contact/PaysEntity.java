package api.sms.astro.infrastructure.entite.contact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p> Objet valeur permettant l'enregistrement d'un pays </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "PAYS")
public class PaysEntity implements Serializable {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column(unique = true, nullable = false)
  private String nom;

  @Column(unique = true)
  private String coordonnee;

  @Column(unique = true)
  private String codeISO;

  @OneToMany(cascade = CascadeType.ALL,
      fetch = FetchType.LAZY,
      mappedBy = "pays", orphanRemoval = true)
  private List<VilleEntity> villes = new ArrayList<>();

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;

  public PaysEntity() {
    super();
  }

  public PaysEntity(String nom) {
    this.nom = nom;
  }

  public PaysEntity(String nom, String coordonnee) {
    this.nom = nom;
    this.coordonnee = coordonnee;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCoordonnee() {
    return coordonnee;
  }

  public void setCoordonnee(String coordonnee) {
    this.coordonnee = coordonnee;
  }

  public List<VilleEntity> getVilles() {
    return villes;
  }

  public void setVilles(
      List<VilleEntity> villes) {
    this.villes = villes;
  }

  public String getCodeISO() {
    return codeISO;
  }

  public void setCodeISO(String codeISO) {
    this.codeISO = codeISO;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaysEntity that = (PaysEntity) o;
    return supprimer == that.supprimer &&
        Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, coordonnee, codeISO, villes, supprimer);
  }
}
