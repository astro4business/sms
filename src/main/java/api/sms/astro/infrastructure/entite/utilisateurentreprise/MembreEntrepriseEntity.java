package api.sms.astro.infrastructure.entite.utilisateurentreprise;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.utilisateurentreprise.objetvaleur.StatutClientEntity;
import java.time.LocalDateTime;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class MembreEntrepriseEntity extends Utilisateur {

  private LocalDateTime dateAdhesion;
  private StatutClientEntity statutClient = StatutClientEntity.BRONZE;

  public MembreEntrepriseEntity(String nom, String prenoms, String motPasse) {
    super(nom, prenoms, motPasse);
  }
}
