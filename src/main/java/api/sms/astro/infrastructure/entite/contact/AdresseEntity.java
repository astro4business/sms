package api.sms.astro.infrastructure.entite.contact;

import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <p> Agrégat permettant d'acceder aux objets valeur {@link Pays} et {@link Ville}
 * </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Embeddable
@Table(name = "ADRESSE")
public class AdresseEntity implements Serializable {

  @Column(length = 38)
  private String adressePostal;

  @ManyToOne
  private PaysEntity pays;

  @ManyToOne
  private VilleEntity ville;

  public AdresseEntity(){
    super();
  }

  public AdresseEntity(String adressePostal) {
    this.adressePostal = adressePostal;
  }

  public String getAdressePostal() {
    return adressePostal;
  }

  public void setAdressePostal(String adressePostal) {
    this.adressePostal = adressePostal;
  }

  public PaysEntity getPays() {
    return pays;
  }

  public void setPays(
      PaysEntity pays) {
    this.pays = pays;
  }

  public VilleEntity getVille() {
    return ville;
  }

  public void setVille(
      VilleEntity ville) {
    this.ville = ville;
  }
}
