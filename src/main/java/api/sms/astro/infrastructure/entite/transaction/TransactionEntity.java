package api.sms.astro.infrastructure.entite.transaction;

import api.sms.astro.core.campagne.domaine.Campagne;
import api.sms.astro.core.transaction.domaine.Prestataire;
import api.sms.astro.infrastructure.entite.campagne.CampagneEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class TransactionEntity {

  private UUID id;

  private BigDecimal fraisTransaction;

  private PrestataireEntity prestataire;
  private CampagneEntity campagne;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
