package api.sms.astro.infrastructure.entite.utilisateurentreprise.objetvaleur;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public enum StatutClientEntity {
  DIAMAND,
  OR,
  ARGENT,
  BRONZE
}
