package api.sms.astro.infrastructure.entite.transaction;

import api.sms.astro.core.transaction.domaine.Transaction;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.utilisateur.UtilisateurEntity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueTransactionEntity implements Serializable {

  private UUID id;
  private TransactionEntity transaction;
  private UtilisateurEntity utilisateur;


  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public HistoriqueTransactionEntity() {
  }

  public HistoriqueTransactionEntity(
      TransactionEntity transaction,
      UtilisateurEntity utilisateur) {
    this.transaction = transaction;
    this.utilisateur = utilisateur;
  }
}
