package api.sms.astro.infrastructure.entite.utilisateur;

import api.sms.astro.core.utilisateur.domaine.Credit;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class HistoriqueCreditEntity implements Serializable {

  private UUID id;
  private CreditEntity credit;
  private UtilisateurEntity utilisateur;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public HistoriqueCreditEntity() {}

  public HistoriqueCreditEntity(CreditEntity credit) {
    this.credit = credit;
  }
}
