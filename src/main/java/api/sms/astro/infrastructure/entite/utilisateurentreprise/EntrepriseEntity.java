package api.sms.astro.infrastructure.entite.utilisateurentreprise;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.campagne.CampagneEntity;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import api.sms.astro.infrastructure.entite.prospect.GroupeProspectEntity;
import api.sms.astro.infrastructure.entite.transaction.TransactionEntity;
import api.sms.astro.infrastructure.entite.utilisateurentreprise.objetvaleur.StatutClientEntity;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class EntrepriseEntity extends Utilisateur {

  private StatutClientEntity statutClient = StatutClientEntity.BRONZE;
  private String description;
  private String raisonSociale;
  private String siret;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  private Set<MembreEntrepriseEntity> membreEntreprise;
  private Set<ContactEntity> contacts;

  private Set<CampagneEntity> campagnes;
  private Set<TransactionEntity> transactions;

  private Set<GroupeProspectEntity> groupeProspects;

  public EntrepriseEntity(String nom, String motPasse) {
    super(nom, motPasse);
  }
}
