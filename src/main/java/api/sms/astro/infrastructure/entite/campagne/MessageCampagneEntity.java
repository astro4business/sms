package api.sms.astro.infrastructure.entite.campagne;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.utilisateur.UtilisateurEntity;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class MessageCampagneEntity {

  private UUID id;

  private String intitule;
  private String messageFormate;

  private UtilisateurEntity destinataire;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
