package api.sms.astro.infrastructure.entite.transaction;

import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.transaction.domaine.GrilleTarifaire;
import api.sms.astro.core.transaction.domaine.HistoriqueGrilleTarifaire;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class PrestataireEntity implements Serializable {

  private UUID id;

  private String nom;

  private ContactEntity contact;

  private Set<GrilleTarifaireEntity> grilleTarifaire;
  private Set<HistoriqueGrilleTarifaireEntity> historiqueGrilleTarifaire;

  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;
}
