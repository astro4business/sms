package api.sms.astro.infrastructure.entite.utilisateur;

import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class MessageEntity implements Serializable {

  private UUID id;

  private String objet;
  private String message;
  private UtilisateurEntity destinataire;

  public boolean modifier = false;
  public boolean supprimer = false;

  private LocalDateTime dateOuvertureMessage;
  private LocalDateTime dateCreation;
  private LocalDateTime dateModification;

  public MessageEntity(String objet, String message,
      UtilisateurEntity destinataire) {
    this.objet = objet;
    this.message = message;
    this.destinataire = destinataire;
  }
}
