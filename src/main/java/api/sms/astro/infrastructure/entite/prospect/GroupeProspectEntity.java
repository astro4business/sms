package api.sms.astro.infrastructure.entite.prospect;

import api.sms.astro.core.prospect.domaine.Prospect;
import api.sms.astro.core.utilisateur.domaine.Utilisateur;
import api.sms.astro.infrastructure.entite.utilisateur.UtilisateurEntity;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 12/08/2020
 **/
public class GroupeProspectEntity implements Serializable {

  private UUID id;
  private Set<ProspectEntity> prospects;
  private UtilisateurEntity utilisateur;

  private boolean supprimer = false;

  public GroupeProspectEntity(
      Set<ProspectEntity> prospects) {
    this.prospects = prospects;
  }
}
