package api.sms.astro.infrastructure.adapter;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.infrastructure.entite.contact.PaysEntity;
import api.sms.astro.infrastructure.mapper.PaysMapper;
import api.sms.astro.infrastructure.repository.PaysJpaRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Repository
public class PaysJpaAdapter implements PaysPortRepository {

  private final PaysJpaRepository paysJpaRepository;
  PaysMapper mapper = Mappers.getMapper(PaysMapper.class);

  public PaysJpaAdapter(
      PaysJpaRepository paysJpaRepository) {
    this.paysJpaRepository = paysJpaRepository;
  }

  @Override
  public void enregistrer(Pays pays) {

    PaysEntity paysEntity = mapper.paysToEntity(pays);
    this.paysJpaRepository.save(paysEntity);
  }

  @Override
  public Pays enregistrerR(Pays pays) {

    PaysEntity paysEntity = mapper.paysToEntity(pays);
    PaysEntity paysEnregistre = this.paysJpaRepository.save(paysEntity);
    Pays paysARetourner = mapper.entityToPays(paysEnregistre);

    return pays;
  }

  @Override
  public void supprimer(Pays pays) {

    pays.setSupprimer(true);
    this.enregistrer(pays);
  }

  @Override
  public List<Pays> afficherListes() {

    List<PaysEntity> paysEntities = this.paysJpaRepository
        .getPaysEntitiesBySupprimerFalse();
    return mapper.entitiesToPays(paysEntities);
  }

  @Override
  public Pays afficherPays(UUID id) {

    PaysEntity paysEntity = this.paysJpaRepository.findPaysEntityById(id);
    return mapper.entityToPays(paysEntity);
  }

  @Override
  public Pays rechercherPaysParNom(String nom) {

    PaysEntity paysEntity = this.paysJpaRepository.findPaysEntityByNom(nom);
    return mapper.entityToPays(paysEntity);
  }
}
