package api.sms.astro.infrastructure.adapter;

import api.sms.astro.core.contact.application.port.ContactPortRepository;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.infrastructure.entite.contact.ContactEntity;
import api.sms.astro.infrastructure.mapper.ContactMapper;
import api.sms.astro.infrastructure.repository.ContactJpaRepository;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Repository
public class ContactJpaAdapter implements ContactPortRepository {

  private final ContactJpaRepository contactJpaRepository;
  ContactMapper mapper = Mappers.getMapper(ContactMapper.class);

  public ContactJpaAdapter(
      ContactJpaRepository contactJpaRepository) {
    this.contactJpaRepository = contactJpaRepository;
  }

  @Override
  public void enregistrer(Contact contact) {

    if (contact.getId() != null){
      ContactEntity contactEntity = this.contactJpaRepository.findContactEntityById(contact.getId());
      if (contactEntity != null) {
        ContactEntity contactEntityClient = mapper.contactToEntity(contact);
        this.contactJpaRepository.save(contactEntityClient);
      }
      else {}
        // Todo : Exception en cas de non existance d'un contact;
    } else {
      ContactEntity contactEntity = mapper.contactToEntity(contact);
      this.contactJpaRepository.save(contactEntity);
    }
  }

  @Override
  public void supprimer(Contact contact) {
    contact.setSupprimer(true);
    this.enregistrer(contact);
  }

  @Override
  public List<Contact> afficherListesContacts() {

    List<ContactEntity> contactEntity = this.contactJpaRepository
        .findContactEntityBySupprimerFalse();
    return mapper.entitiesToContacts(contactEntity);
  }
}
