//package api.sms.astro.infrastructure.adapter;
//
//import api.sms.astro.core.contact.domaine.Adresse;
//import api.sms.astro.core.utilisateur.application.port.AdressePortRepository;
//import api.sms.astro.infrastructure.entite.contact.AdresseEntity;
//import api.sms.astro.infrastructure.repository.AdresseJpaRepository;
//import org.springframework.beans.BeanUtils;
//import org.springframework.stereotype.Repository;
//
///**
// * <p>  </p>
// *
// * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
// **/
//@Repository
//public class AdresseJpaAdapter implements AdressePortRepository {
//
//  private final AdresseJpaRepository adresseJpaRepository;
//
//  public AdresseJpaAdapter(
//      AdresseJpaRepository adresseJpaRepository) {
//    this.adresseJpaRepository = adresseJpaRepository;
//  }
//
//  @Override
//  public void enregistrer(Adresse adresse) {
//    AdresseEntity adresseEntity = new AdresseEntity();
//    BeanUtils.copyProperties(adresse, adresseEntity);
//    this.adresseJpaRepository.save(adresseEntity);
//  }
//}
