package api.sms.astro.rest;

import api.sms.astro.core.contact.application.casutilisation.AfficherListesPays;
import api.sms.astro.core.contact.application.casutilisation.AfficherListesVille;
import api.sms.astro.core.contact.application.casutilisation.CreerPays;
import api.sms.astro.core.contact.application.casutilisation.CreerVille;
import api.sms.astro.core.contact.application.casutilisation.ModifierPays;
import api.sms.astro.core.contact.application.casutilisation.ModifierVille;
import api.sms.astro.core.contact.application.commande.CreerContactCommande;
import api.sms.astro.core.contact.application.gestionnairecommande.CreerContactGestionnaire;
import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.application.port.VillePortRepository;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import api.sms.astro.core.utilisateur.GestionnaireCommande;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@RestController
public class ApiTestController {

  private final GestionnaireCommande<CreerContactCommande> gestionnaireCreerContact;
  private final CreerVille creerVille;
  private final CreerPays creerPays;
  private final AfficherListesVille afficherListesVille;
  private final AfficherListesPays afficherListesPays;
  private final ModifierVille modifierVille;
  private final ModifierPays modifierPays;

  public ApiTestController(ContactPortRepository contactPortRepository,
      VillePortRepository villePortRepository, PaysPortRepository paysPortRepository) {
    this.gestionnaireCreerContact = new CreerContactGestionnaire(
        contactPortRepository);
    this.creerVille = new CreerVille(villePortRepository);
    this.creerPays = new CreerPays(paysPortRepository);
    this.modifierVille = new ModifierVille(villePortRepository);
    this.modifierPays = new ModifierPays(paysPortRepository);
    this.afficherListesVille = new AfficherListesVille(villePortRepository);
    this.afficherListesPays = new AfficherListesPays(paysPortRepository);
  }

  @GetMapping("/")
  public Contact testContact() {
    Contact contact = new Contact("59087887");
    contact.setNumeroFixe("57987546546");
    contact.setEmail("dfsdfsdfs@dsfs.dsfs");

    return contact;
  }

  @PostMapping("/")
  @ResponseStatus(HttpStatus.CREATED)
  public void creerContact(@RequestBody CreerContactCommande commande) {
    this.gestionnaireCreerContact.execute(commande);
  }

  @PostMapping("/afficher/villes")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public List<Ville> afficherVilles() {
    List<Ville> ville = this.afficherListesVille.afficher();
    return ville;
  }

  @PostMapping("/afficher/pays")
  @ResponseStatus(HttpStatus.FOUND)
  public ResponseEntity< List<Pays> > afficherPays() {
    return new ResponseEntity<>( this.afficherListesPays.afficher(), HttpStatus.FOUND );
  }

  @PostMapping("/creer/ville")
  @ResponseStatus(HttpStatus.CREATED)
  public void creerVille(@RequestBody Ville ville) {
    this.creerVille.creer(ville);
  }

  @PostMapping("/creer/pays")
  @ResponseStatus(HttpStatus.CREATED)
  public void creerPays(@RequestBody Pays pays) {
    this.creerPays.creer(pays);
  }

  @PostMapping("/modifier/pays")
  @ResponseStatus(HttpStatus.CREATED)
  public void modifierPays(@RequestBody Pays pays) {
    this.modifierPays.modifier(pays);
  }

  @PostMapping("/modifier/ville")
  @ResponseStatus(HttpStatus.CREATED)
  public void modifierVille(@RequestBody Ville ville) {
    this.modifierVille.modifier(ville);
  }
}
