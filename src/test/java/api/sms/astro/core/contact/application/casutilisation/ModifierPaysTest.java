package api.sms.astro.core.contact.application.casutilisation;

import static org.junit.jupiter.api.Assertions.*;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 23/08/2020
 **/
@ExtendWith(MockitoExtension.class)
class ModifierPaysTest {

  @Mock
  PaysPortRepository paysPortRepository;

  Pays pays = new Pays();
  ModifierPays modifierPays;

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test pour la modification d'un pays")
  void modifier() {

    // Given
    Mockito.doNothing().when(this.paysPortRepository).enregistrer(Mockito.any(Pays.class));
    modifierPays = new ModifierPays(this.paysPortRepository);

    // When
    this.modifierPays.modifier(this.pays);

    // Then
    Mockito.verify(this.paysPortRepository, Mockito.times(1)).enregistrer(Mockito.isA(Pays.class));
  }
}