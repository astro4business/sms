package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 23/08/2020
 **/
@ExtendWith(MockitoExtension.class)
class CreerPaysTest {

  @Mock
  PaysPortRepository paysPortRepository;

  Pays pays = new Pays();
  Ville ville = new Ville();

  @BeforeEach
  void setUp() {

    pays.setNom("pays");
    pays.setCodeISO("codeiso");
    pays.setCoordonnee("coordonnee");

    ville.setNom("ville");
    ville.setCoordonnee("coordonnee_ville");
    ville.setCodeISO("codeiso_ville");

    pays.ajouterVille(ville);
  }

  @Test
  @DisplayName("Test pour la création d'un pays")
  void testCreationPays() {

    // Given
    CreerPays creerPays = new CreerPays(this.paysPortRepository);

    Mockito.doNothing().when(this.paysPortRepository)
        .enregistrer(Mockito.any(Pays.class));
    Mockito.when(
        this.paysPortRepository.rechercherPaysParNom(Mockito.isA(String.class)))
        .thenReturn(pays);

    // When
    creerPays.creer(this.pays);

    // Then
    Mockito.verify(this.paysPortRepository, Mockito.times(2))
        .enregistrer(Mockito.any(Pays.class));
    Mockito.verify(this.paysPortRepository, Mockito.times(1))
        .rechercherPaysParNom(Mockito.isA(String.class));
  }
}