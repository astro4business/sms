package api.sms.astro.core.contact.application.commande;

import static org.assertj.core.api.Assertions.assertThat;

import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
class CreerContactCommandeTest {

//  CreerContactCommande commande;
  Validator validator;

  @BeforeEach
  void setUp() {
//    this.commande = new CreerContactCommande();
  }

  @Test
  void commandeNotBlank() {

    CreerContactCommande commande = new CreerContactCommande();
//    this.commande = new CreerContactCommande();

    Validator validator = Validation.buildDefaultValidatorFactory()
        .getValidator();

    commande.setAdressePostal("aeaealml@sds.cvv");
//    commande.setEmail("aeaealml@sds.cvv");
//    commande.setNumeroFixe("545648788");
//    commande.setNumeroTelephone("5654654871");
    commande.setPays(new Pays());
    commande.setVille(new Ville());

    Set<ConstraintViolation<CreerContactCommande>> violations = validator
        .validate(commande);

    System.out.println(violations);

    assertThat(violations.size()).isEqualTo(0);
  }
}