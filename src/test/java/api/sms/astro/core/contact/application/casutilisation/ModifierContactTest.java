package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.commande.ModifierContactCommande;
import api.sms.astro.core.contact.application.gestionnairecommande.ModifierContactGestionnaire;
import api.sms.astro.core.contact.domaine.Contact;
import api.sms.astro.core.contact.domaine.Pays;
import api.sms.astro.core.contact.domaine.Ville;
import api.sms.astro.core.contact.application.port.ContactPortRepository;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
@ExtendWith(MockitoExtension.class)
class ModifierContactTest {

  @Mock
  ContactPortRepository contactPortRepository;

  @BeforeEach
  void setUp() {
  }

  @Test
  @DisplayName("Test pour la modification d'un contact.")
  void modifier() {

    //Given
    ModifierContactCommande commande = new ModifierContactCommande();

    Pays pays = new Pays();
    Ville ville = new Ville();
    pays.ajouterVille(ville);

    commande.setPays(pays);
    commande.setVille(ville);
    commande.setId(UUID.randomUUID());

    ModifierContact modifierContact = new ModifierContact(this.contactPortRepository);
    ModifierContactGestionnaire gestionnaireCommande = new ModifierContactGestionnaire(modifierContact);

    Mockito.doNothing().when(this.contactPortRepository).enregistrer(Mockito.any(Contact.class));

    // When
    gestionnaireCommande.execute(commande);

    // Then
    Mockito.verify(this.contactPortRepository, Mockito.times(1)).enregistrer(Mockito.isA(Contact.class));
  }
}