package api.sms.astro.core.contact.application.casutilisation;

import api.sms.astro.core.contact.application.port.PaysPortRepository;
import api.sms.astro.core.contact.domaine.Pays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 23/08/2020
 **/
@ExtendWith(MockitoExtension.class)
class SupprimerPaysTest {

  @Mock
  PaysPortRepository paysPortRepository;

  SupprimerPays supprimerPays;

  @BeforeEach
  void setUp() {

    // Given
    supprimerPays = new SupprimerPays(this.paysPortRepository);
  }

  @Test
  @DisplayName("Test pour la suppression d'un pays")
  void supprimer() {

    // Given
    Pays pays = new Pays();
    Mockito.doNothing().when(this.paysPortRepository)
        .supprimer(Mockito.any(Pays.class));

    // When
    this.supprimerPays.supprimer(pays);

    // Then
    Mockito.verify(this.paysPortRepository, Mockito.times(1))
        .supprimer(Mockito.isA(Pays.class));
  }
}